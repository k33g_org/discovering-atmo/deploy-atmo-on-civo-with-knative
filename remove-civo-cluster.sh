#!/bin/bash
#CLUSTER_NAME="tinypanda"
CLUSTER_NAME="bigpanda"
CLUSTER_REGION=NYC1
export KUBECONFIG=$PWD/config/k3s.yaml

civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
civo kubernetes remove ${CLUSTER_NAME} --region=${CLUSTER_REGION} --yes 

rm ./config/k3s.yaml
rm ./config/url.txt
