FROM gitpod/workspace-full

RUN sudo apt-get update && \
    sudo apt-get install gettext -y  && \
    sudo apt-get install -y sshpass && \
    brew install derailed/k9s/k9s && \
    brew install helm && \
    brew install kubernetes-cli helm && \
    brew install kompose && \
    brew install kn


USER gitpod

# Tools
RUN brew tap suborbital/subo && \
    brew install subo && \
    brew tap civo/tools && \
    brew install civo && \
    brew install exa && \
    brew install bat && \
    brew install httpie && \
    brew install hey
