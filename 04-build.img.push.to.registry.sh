#!/bin/bash
cd services
docker login registry.gitlab.com -u k33g -p ${GITLAB_TOKEN_ADMIN}
docker build -t atmo-services . 
docker tag atmo-services registry.gitlab.com/k33g_org/discovering-atmo/deploy-atmo-on-civo-with-knative/atmo-services:latest
docker push registry.gitlab.com/k33g_org/discovering-atmo/deploy-atmo-on-civo-with-knative/atmo-services:latest