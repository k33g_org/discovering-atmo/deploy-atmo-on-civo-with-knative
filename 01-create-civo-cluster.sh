#!/bin/bash
CLUSTER_NAME="bigpanda"
CLUSTER_SIZE="g3.k3s.large"
#CLUSTER_NAME="tinypanda"
#CLUSTER_SIZE="g3.k3s.xsmall"
CLUSTER_NODES=1
CLUSTER_REGION=NYC1
export KUBECONFIG=$PWD/config/k3s.yaml

echo "🤖[1/6] creating Civo Cluster: ${CLUSTER_NAME}" 

civo apikey add civo-key ${CIVO_API_KEY}
civo apikey current civo-key
# Knative provides its own network layer, so we need to install K3S without Traefik
civo kubernetes create ${CLUSTER_NAME} --size=${CLUSTER_SIZE} --nodes=${CLUSTER_NODES} --region=${CLUSTER_REGION} --remove-applications Traefik --wait


# Cluster size g3.k3s.medium / g3.k3s.large / g3.k3s.xlarge 

echo "📝[2/6]  save the KUBECONFIG file of the ${CLUSTER_NAME} cluster to ./config/k3s.yaml "

civo --region=${CLUSTER_REGION} kubernetes config ${CLUSTER_NAME} > ./config/k3s.yaml

echo "🌍[3/6]  get the cluster url (./config/url.txt)"

URL=$(kubectl cluster-info | grep -E 'Kubernetes master|Kubernetes control plane' | awk '/http/ {print $NF}') 
# Removing ANSI color codes from text stream
NEW_URL=$(echo $URL | sed 's/\x1b\[[0-9;]*m//g')
echo "$NEW_URL"
echo "$NEW_URL" > ./config/url.txt
